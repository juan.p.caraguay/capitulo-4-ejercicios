# autor = " juan caraguay"
# email = " juan.p.caraguay@unl.eu.ec"
# Desplaza la ultima linea del programa anterior hacia arriba, de modo que la llamada a la funcion
# aparezca antes de las definiciones
# ejecuta el programa y observa que mensaje de error obtienes


def muestra_estribillo() :
    print('Si queremos enseñar a los niños a pensar.')
    print('Debemos enseñarles antes a inventar.')


def repite_estribillo() :
    muestra_estribillo()
    muestra_estribillo()

repite_estribillo()
print('Si queremos enseñar a los niños a pensar.')
print('Debemos enseñarles a inventar.')


# se presenta los mensajes de la funcion repite estribillo anidada en la funcion muestra estribilo
#y las sentencias de la funcion muestra estribillo sin llamar a la funcion.

